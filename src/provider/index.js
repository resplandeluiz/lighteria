import React, { useState, createContext } from 'react'

export const DataContext = createContext()

export const Provider = ({ children }) => {

    const [itemsCheckout, setItemsCheckout] = useState([])

    return (
        <DataContext.Provider
            value={{
                itemsCheckout,
                addItem: newItem => {

                    let copyItemsCheckout = [...itemsCheckout]
                    let itemFilter = copyItemsCheckout.find(item => item.id === newItem.id)


                    if (itemFilter) {
                        itemFilter.quantity = itemFilter.quantity + 1

                    } else {
                        newItem.quantity = 1
                        copyItemsCheckout = [...copyItemsCheckout, newItem]
                    }

                    setItemsCheckout(copyItemsCheckout)

                }
            }}
        >
            {children}
        </DataContext.Provider>
    )
}
