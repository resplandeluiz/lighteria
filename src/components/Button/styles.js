
import { StyleSheet } from 'react-native';

import { WHITE, LIGHTBLUE, FONT_FAMILY_SEMI_BOLD, FONT_SIZE_MEDIUM } from '../../styles/styles';

export default styles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: LIGHTBLUE,
        padding: 20,
        borderRadius: 6,
    },
    buttonText: {
        textAlign: "center",
        fontFamily: FONT_FAMILY_SEMI_BOLD,
        fontSize: FONT_SIZE_MEDIUM,
        color: WHITE,
    },
});