
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles'


export default function Button({ titulo, onPress, disabled = false }) {
    return (
        <TouchableOpacity onPress={onPress} disabled={disabled}>
            <View style={styles.buttonContainer}>
                <Text style={styles.buttonText}>{titulo}</Text>
            </View>
        </TouchableOpacity>
    );
}
