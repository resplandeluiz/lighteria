import React, { useContext } from 'react'
import { useNavigation } from '@react-navigation/core'
import { Image, TouchableOpacity, View, Text } from 'react-native'

import { DataContext } from '../../provider'

import styles from './styles'

export const ShoppingBag = () => {

    const { itemsCheckout } = useContext(DataContext)

    const navigation = useNavigation()

    return (
        <TouchableOpacity style={styles.containerImage} onPress={() => navigation.push("Checkout")}>
            <View>
                <Image source={require('../../assets/images/icone-sacola.png')} style={styles.image} />
            </View>

            {itemsCheckout.length > 0 &&
                <View style={styles.containerQuantity}>
                    <Text style={styles.textQuantity}>
                        {itemsCheckout.reduce((amout, atual) => amout + atual.quantity, 0)}
                    </Text>
                </View>}

        </TouchableOpacity>
    )
}



