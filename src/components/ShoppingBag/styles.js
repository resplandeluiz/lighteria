import { StyleSheet } from 'react-native'
import { FONT_FAMILY_SEMI_BOLD, WHITE } from '../../styles/styles'


export default styles = StyleSheet.create({

  containerImage:
  {
    backgroundColor: "#FFF",
    padding: 18,
    borderRadius: 30
  },

  image: {
    width: 30,
    height: 30
  },

  containerQuantity: {

    backgroundColor: "red",
    borderRadius: 100,
    marginTop: -22,
    marginLeft: 11

  },

  textQuantity: {
    textAlign: "center",
    padding: 4,
    fontSize: 10,
    fontFamily: FONT_FAMILY_SEMI_BOLD,
    fontWeight: "bold",
    color: WHITE
  }


})