import React from 'react'
import { View, FlatList } from 'react-native'

import data from '../../utils/data'

import HeaderListProduct from './Components/Header'
import ItemProduct from './Components/ItemProduct'
import styles from './styles'

const ListProduct = () => {
    return (
        <View style={styles.container}>
            <FlatList
                numColumns={2}
                data={data}
                renderItem={({ item }) => <ItemProduct {...item} />}
                keyExtractor={(item) => item.id}
                showsVerticalScrollIndicator={false}
                ListHeaderComponent={<HeaderListProduct />}
            />
        </View>
    )
}

export default ListProduct;