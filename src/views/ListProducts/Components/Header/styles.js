import { StyleSheet } from 'react-native'
import { BACKGROUND_COLOR, FONT_FAMILY_EXTRA_BOLD, FONT_FAMILY_REGULAR } from '../../../../styles/styles'

export default styles = StyleSheet.create({

  containerTitle:
  {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 24,
    paddingHorizontal: 6

  },

  textTitle:
  {
    fontFamily: FONT_FAMILY_EXTRA_BOLD,
    fontSize: 28,
    fontWeight: "bold"
  },

  containerImage:
  {
    backgroundColor: "#FFF",
    padding: 18,
    borderRadius: 30
  },
  
  image: {
    width: 30,
    height: 30
  },

  separator: {
    borderWidth: 0.5,
    borderColor: "#A1A5AA"
  },

  containerText:
  {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: -45
  },

  textDescription:
  {
    padding: 34,
    backgroundColor: BACKGROUND_COLOR,
    fontSize: 16,
    fontFamily: FONT_FAMILY_REGULAR,
    color: "#A1A5AA"

  }
})