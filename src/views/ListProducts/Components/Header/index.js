import React from 'react'
import { Text, View } from 'react-native'

import styles from './styles'

import { ShoppingBag } from '../../../../components/ShoppingBag'


const HeaderListProduct = () => {
    return (
        <>

            <View style={styles.containerTitle}>

                <Text style={styles.textTitle}>Lighteria</Text>

                <ShoppingBag />

            </View>

            <View>
                <View style={styles.separator} />

                <View style={styles.containerText}>
                    <Text style={styles.textDescription}>Categorias</Text>
                </View>
            </View>
        </>

    )
}

export default HeaderListProduct;