import { StyleSheet } from 'react-native'
import { FONT_FAMILY_EXTRA_BOLD} from '../../../../styles/styles'

export default styles = StyleSheet.create({

    containterItem: {
        
        height: 168,
        backgroundColor: "#FFF",
        borderRadius: 10,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        margin: 8
        
    },

    imagem: {
        
        height: 84,
        resizeMode: "contain"

    },

    itemTitle: {

        marginTop: 8,
        fontFamily: FONT_FAMILY_EXTRA_BOLD,
        fontWeight: "bold",
        fontSize: 14,
        color: "#848486"

    }

})