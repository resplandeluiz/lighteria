import React from 'react'
import { useNavigation } from '@react-navigation/core'
import { Text, Image, TouchableOpacity } from 'react-native'

import styles from './styles'

const ItemProduct = ({ imagem, titulo, estudio, itemDesc, itemName,  id, preco }) => {

    const navigation = useNavigation()

    return (
        <TouchableOpacity style={styles.containterItem} onPress={() => navigation.push("DetailProduct", { imagem, titulo, estudio, itemDesc, itemName, preco, id })}>

            <Image source={imagem} style={styles.imagem} />
            <Text style={styles.itemTitle}>{titulo}</Text>

        </TouchableOpacity>
    )
}

export default ItemProduct