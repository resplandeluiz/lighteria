import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get("window")

export default styles = StyleSheet.create({

    imgBackground: {
        width: width,
        height: "100%"
    },

    headerDetail: {
        flexDirection: "row",
        justifyContent: "space-between"
    },

    containerBG: {
        flex: 6
    },

    imgArrow: {
        width: 24,
        height: 24,
        marginTop: 36,
        marginLeft: 24
    },

    containterShoppingBag: { padding: 18 }


})