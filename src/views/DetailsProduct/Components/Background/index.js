import React from 'react'
import { useNavigation } from '@react-navigation/core'
import { Image, ImageBackground, View, TouchableOpacity } from 'react-native'

import styles from './styles'

import { ShoppingBag } from '../../../../components/ShoppingBag'

const Background = () => {

    const imgSrc = require("../../../../assets/images/bgimg.jpg")
    const navigation = useNavigation()

    return (
        <View style={styles.containerBG}>

            <ImageBackground
                resizeMode={"cover"}
                source={imgSrc}
                style={styles.imgBackground}
            >
                <View style={styles.headerDetail}>

                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image source={require("../../../../assets/images/flecha-esquerda.png")} style={styles.imgArrow} />
                    </TouchableOpacity>

                    <View style={styles.containterShoppingBag}>
                        <ShoppingBag />
                    </View>

                </View>

            </ImageBackground>

        </View>
    )

}

export default Background;