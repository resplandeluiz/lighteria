import React from 'react'
import { useNavigation } from '@react-navigation/core'
import { View, Text, Image } from 'react-native'

import styles from './styles'

import { formatValue } from '../../../../utils/utils'
import Button from '../../../../components/Button'

import { DataContext } from '../../../../provider/index'
import { useContext } from 'react'

const ItemDescription = ({
    imagem,
    titulo,
    estudio,
    itemDesc,
    itemName,
    preco,
    id
}) => {

    const { addItem } = useContext(DataContext)
    const navigation = useNavigation()

    return (
        <View style={styles.itemContainer}>
            <View style={styles.itemPosition}>
                <View style={styles.item}>

                    <View style={styles.textPosition}>
                        <View>
                            <Text style={styles.textTop}>{estudio}</Text>
                            <Text style={styles.textMiddle}>{itemName}</Text>
                            <Text style={styles.textBottom}>{titulo}</Text>


                        </View>

                        <Image source={imagem} style={styles.image} />
                    </View>

                    <Text style={styles.textDescription}>
                        {itemDesc}
                    </Text>

                    <View style={styles.footer}>
                        <Text style={styles.priceText}>
                            {formatValue(preco)}
                        </Text>

                        <Button titulo={"COMPRAR"} onPress={() => addItem({
                            imagem,
                            titulo,
                            estudio,
                            itemDesc,
                            itemName,
                            preco,
                            id
                        })} />

                    </View>

                </View>
            </View>
        </View>
    )

}

export default ItemDescription;