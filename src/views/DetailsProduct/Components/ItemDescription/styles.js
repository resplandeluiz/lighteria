import { StyleSheet } from 'react-native'
import { BLACK, FONT_FAMILY_BOLD, FONT_FAMILY_REGULAR } from '../../../../styles/styles'

export default styles = StyleSheet.create({

    itemContainer: { flex: 4 },

    itemPosition: {
        marginTop: -60,
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center"
    },

    item: {
        backgroundColor: "#FFFFFF",
        borderRadius: 30,
        padding: 28,
        width: '80%',
        elevation: 4
    },

    textPosition: {
        flexDirection: "row",
        justifyContent: "space-between",
    },

    textTop: {
        fontFamily: FONT_FAMILY_BOLD,
        fontSize: 16,
        marginBottom: 4,
        fontWeight: "bold"
    },

    textMiddle: {
        fontFamily: FONT_FAMILY_BOLD,
        fontSize: 18,
        marginBottom: 4,
        fontWeight: "bold"
    },

    textBottom: {
        fontFamily: FONT_FAMILY_REGULAR,
        fontSize: 14,
        marginBottom: 12,
        color: "#CACACA"
    },

    image: {
        width: 50,
        height: 72,
        resizeMode: 'contain'
    },

    textDescription: {
        fontFamily: FONT_FAMILY_REGULAR,
        fontSize: 14,
        marginTop: 10,
        color: "#ACAAAB"
    },

    footer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10,
        alignItems: "center"
    },

    priceText: {
        fontFamily: FONT_FAMILY_BOLD,
        fontSize: 18,
        color: BLACK,
        fontWeight: "bold"
    }

})