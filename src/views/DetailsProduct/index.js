import React from 'react'
import { View, Text } from 'react-native'
import styles from './styles'

import Background from './Components/Background'
import ItemDescription from './Components/ItemDescription'

const DetailProduct = ({ route }) => {

    const { imagem, titulo, estudio, itemDesc, itemName, preco, id } = route.params
    
    return (

        <View style={styles.container}>

            <Background />

            <ItemDescription
                imagem={imagem}
                titulo={titulo}
                estudio={estudio}
                itemDesc={itemDesc}
                itemName={itemName}
                preco={preco}
                id={id}
            />

        </View>

    )

}

export default DetailProduct;