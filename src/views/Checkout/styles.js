import { StyleSheet } from 'react-native'
import { FONT_FAMILY_SEMI_BOLD, LIGHTBLUE } from '../../styles/styles'

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 24
    }
    ,
    textTitle: {
        fontFamily: FONT_FAMILY_SEMI_BOLD,
        fontWeight: "bold",
        fontSize: 24,
        marginBottom: 10
    },

    textTotal: {
        fontSize: 18,
        fontFamily: FONT_FAMILY_SEMI_BOLD,
        fontWeight: "bold",
        marginVertical: 36
    },

    textKeepBuy: {
        fontFamily: FONT_FAMILY_SEMI_BOLD,
        fontWeight: "bold",
        fontSize: 14,
        color: LIGHTBLUE,
        marginTop: 20,
        textAlign: "center"
    }
})