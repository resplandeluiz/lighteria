import React, { useContext } from 'react'
import { useNavigation } from '@react-navigation/core'
import { View, Text, TouchableOpacity } from 'react-native'
import Button from '../../components/Button'
import { DataContext } from '../../provider'
import { formatValue } from '../../utils/utils'
import styles from './styles'

import CheckoutItem from './CheckoutItem'

const Checkout = () => {

    const navigation = useNavigation()
    const { itemsCheckout } = useContext(DataContext)

    const valorTotal = itemsCheckout.reduce((acumulado, atual) => acumulado + atual.quantity * atual.preco, 0)

    const Titulo = ({ children }) => <Text children={children} style={styles.textTitle} />

    const Total = ({ children }) => <Text children={children} style={styles.textTotal} />

    return (
        <View style={styles.container}>

            <Titulo>Checkout</Titulo>

            {itemsCheckout.map(item => <CheckoutItem item={item} />)}

            <Total>{formatValue(valorTotal)}</Total>

            <Button titulo={"FINALIZAR COMPRA"} disabled={valorTotal <= 0} />

            <TouchableOpacity onPress={() => navigation.push("Products")}>
                <Text style={styles.textKeepBuy}>Continuar comprando</Text>
            </TouchableOpacity>

        </View>
    )
}


export default Checkout