import React, { useContext } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { formatValue } from '../../../utils/utils'

import styles from './styles'

const CheckoutItem = ({ item }) => {

    return (
        <View style={styles.container}>

            <View style={styles.imageContainer}>
                <Image source={item.imagem} style={styles.image} />
            </View>

            <View style={styles.descContainer}>
                <Text style={styles.descTextTop}>{item.itemName}</Text>
                <Text style={styles.descTextBottom}>{item.titulo}</Text>
            </View>


            <View style={styles.priceContainer}>
                <Text style={styles.textPrice}>{formatValue(item.preco * item.quantity)}</Text>
                <Text style={styles.textQuantity}>Qtd: {item.quantity}</Text>
            </View>


        </View>
    )
}


export default CheckoutItem