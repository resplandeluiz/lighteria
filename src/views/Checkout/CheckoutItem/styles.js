import { StyleSheet } from 'react-native'
import { FONT_FAMILY_LIGHT, FONT_FAMILY_REGULAR } from '../../../styles/styles'
// import { } from '../../styles/styles'

export default styles = StyleSheet.create({

    container: {
        backgroundColor: "#FFFFFF",
        borderRadius: 10,
        marginBottom: 4,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },

    imageContainer: {
        flex: 20,
        backgroundColor: "#F7F8FC",
        padding: 18,
        justifyContent: "center",
        alignItems: "center"
    },

    image: {
        width: 40,
        height: 50,
        resizeMode: "contain"
    },

    descContainer: {
        flex: 25,
        padding: 18,
        justifyContent: "center"
    },

    descTextTop: {
        fontFamily: FONT_FAMILY_REGULAR,
        fontSize: 16
    },

    descTextBottom: {
        fontFamily: FONT_FAMILY_LIGHT,
        fontSize: 14
    },

    priceContainer: {
        flex: 35,
        padding: 18,
        justifyContent: "center",
        alignItems: "flex-end"
    },

    textPrice: {
        fontFamily: FONT_FAMILY_REGULAR
    },

    textQuantity: {
        textAlign: "center",
        padding: 4,
        fontSize: 10,
        // color: "#FFFFFF"
    }

})