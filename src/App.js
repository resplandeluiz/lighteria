import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { BACKGROUND_COLOR } from './styles/styles'

import { Provider } from './provider'

import ListProducts from './views/ListProducts'
import DetailProduct from './views/DetailsProduct'
import Checkout from './views/Checkout'


const Stack = createStackNavigator()

const App = () => {

  return (
    <NavigationContainer>

      <Provider>

        <SafeAreaView style={styles.container}>

          <Stack.Navigator initialRouteName={"Products"}>

            <Stack.Screen name="Products" component={ListProducts} options={{ headerShown: false }} />

            <Stack.Screen name="DetailProduct" component={DetailProduct} options={{ headerShown: false }} />

            <Stack.Screen name="Checkout" component={Checkout} options={{ headerShown: false }} />

          </Stack.Navigator>

        </SafeAreaView>

      </Provider>

    </NavigationContainer>
  )
}

const styles = StyleSheet.create({

  container:
  {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR
  }

})

export default App;
